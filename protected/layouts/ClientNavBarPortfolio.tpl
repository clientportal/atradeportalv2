<div class="container-fluid nav-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
			<div>
                <div class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                         <li <%= $_GET["page"] == 'client.CompleteViewByAccount' ? 'class="active"' : "" ; %> >
                            <a href="index.php?page=client.CompleteViewByAccount">ACCOUNTS</a>
                        </li>
                         <li <%= $_GET["page"] == 'client.NewEquityOrder' ? 'class="active"' : "" ; %> >
                            <a href="index.php?page=client.NewEquityOrder"> TRADING </a>
                        </li>
						 <li <%= $_GET["page"] == 'client.PortfolioHoldings' ? 'class="active"' : "" ; %> >
                            <a href="index.php?page=client.PortfolioHoldings"> PORTFOLIOS</a>
                        </li>
                         <li <%= $_GET["page"] == 'client.SecurityOverview' ? 'class="active"' : "" ; %> >
                            <a href="index.php?page=guest.SecurityOverview">RESEARCH</a>
                        </li>
                    </ul>
                </div>
           