
<div class="col-md-10 pull-right">
	<ul class="header-navigation">
		<li class="current"><a href="index.php?Home">Home</a>
		</li>
		<li><a href="index.php?page=guest.AboutUs">About</a>
		</li>
		<li><a href="index.php?page=guest.AboutUs">How it works</a>
		</li>
		<li><a href="index.php?page=guest.OpenAccount">Open an Account</a>
		</li>
		<li><a href="index.php?page=guest.SecurityOverview">Research & Quotes</a>
		</li>
		

		<li><a href="#" class="signin btn red">Sign In</a>

			<div id="frmsignin" class="form-group form-horizontal">
				<p>
					<com:TCustomValidator ControlToValidate="Password" ErrorMessage="Login failed..." ValidationGroup="Login" Display="Dynamic" OnServerValidate="validateUser" />
					<com:TRequiredFieldValidator ControlToValidate="Username" ErrorMessage="The username was not provided" ValidationGroup="Login" FocusOnError="true" FocusElementID="Username" Display="None" />
					<com:TRequiredFieldValidator ControlToValidate="Password" ErrorMessage="The password was not provided" ValidationGroup="Login" Display="None" />
				</p>
				<com:TTextBox ID="Username" ValidationGroup="Login" cssClass="form-control" Attributes.placeholder="User ID" />
				<com:TTextBox ID="Password" ValidationGroup="Login" cssclass="form-control" Attributes.placeholder="Password" TextMode="Password" />

				<p style="margin-top:3px;">
					<input type="checkbox">&nbsp;Remember me
					<com:TButton Text="Sign In" OnClick="loginButtonClicked" cssClass="btn yellow pull-right"  CausesValidation="true" ValidationGroup="Login" />

				</p>
			</div>
		</li>
	</ul>
</div>