<div class="container">
    <div class="row">
        <div class="col-md-6 search-bar">
            <div class="form">
                <div>
                    <com:TTextBox ID="Symbol" ReadOnly="false"
                              ValidationGroup="GetQuote"
                              Attributes.placeholder="Get Stock Quote" Attributes.type="search"/>
                     <com:TRequiredFieldValidator
                            ControlToValidate="Symbol"
                            ErrorMessage="Symbol required .."
                            Display="Dynamic"
                            ValidationGroup="GetQuote"/>
                     <com:TButton Text="Go" OnClick="getStockDetails"
                                                 ValidationGroup="GetQuote" CausesValidation="true"/>
                     <span class="cl"></span>
                </div>
            </div>
        </div>
    </div>
</div>