<div class="container-fluid nav-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<div>
                <div class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
                <com:TControl Visible="<%= !$this->User->IsGuest %>">
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="index.php?page=client.CompleteViewByAccount">ACCOUNTS</a>
                        </li>
                        <li <%= $_GET["page"] == 'client.NewEquityOrder' ? 'class="active"' : "" ; %> >
                            <a href="index.php?page=client.NewEquityOrder"> TRADING </a>
                        </li>
						 <li <%= $_GET["page"] == 'client.PortfolioHoldings' ? 'class="active"' : "" ; %> >
                            <a href="index.php?page=client.PortfolioHoldings"> PORTFOLIOS</a>
                        </li>
                        <li class="active">
                            <a href="#">RESEARCH</a>
                        </li>
                    </ul>
                </div>
                </com:TControl>

                <com:TControl Visible="<%= $this->User->IsGuest %>">
                <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
										<li>
                                              <a href="index.php?page=guest.AboutMTrader">About ClientPortal</a>
                                          </li> 
										  <li>
                                              <a href="index.php?page=guest.WhyMTrader">Why ClientPortal</a>
                                          </li>
                                          <li>
                                              <a href="index.php?page=guest.HowItworks">How it Works</a>
                                          </li>
											<li>
                                              <a href="index.php?page=guest.SecurityOverview">Resources</a>
                                          </li>
                          <!--  <li>
                                <a href="index.php?page=guest.AboutMTrader">ABOUT AFRINVESTOR</a>
                            </li>
                          
                            <li>
                                <a href="index.php?page=guest.SecurityOverview">RESOURCES</a>
                            </li>
                            <li class="dropdown">
                                <a href="#">INVESTMENT PRODUCTS</a>                            
							</li>-->
                        </ul>
                    </div>
                    </com:TControl>
