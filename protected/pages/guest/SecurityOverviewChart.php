<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

class SecurityOverviewChart extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Security Chart ";


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {

                $webservice = new WebServiceClient(
                    Prado::getApplication()->Parameters['mcs-wsdl'],
                    Prado::getApplication()->Parameters['ws-username'],
                    Prado::getApplication()->Parameters['ws-password']);

                //Get the portfolio is not already bound
                $securities = $session['__securities__'];
                if ($securities == null) {
                    $securities = $webservice->getWebService()->findActiveSecurities("NSE", 0, 1000);
                    $session['__securities__'] = $securities;
                }

                //Chart type
                //$session['__chart_type__'] = "area";
                $session['__chart_type__'] = "column";
                //$session['__chart_type__'] = "candlestick";

                //Chart series
                $session['__chart_series__'] = "close";


                if (isset($securities->item) && count($securities->item) > 0) {

                    $this->securities->DataSource = $session['__securities__']->item;
                    $this->securities->dataBind();

                    $request = $this->getApplication()->getRequest();
                    $symbol = $request['symbol'];
                    if ($symbol != null) {
                        try {
                            $sec = $webservice->getWebService()->findSecurityOverviewByCode(
                                Prado::getApplication()->Parameters['default-exchange'],
                                $symbol);
                            $this->securities->SelectedValue = $sec->securityId;
                            $this->renderSecurityData($sec->securityId);
                        } catch (SoapFault $e) {
                            $this->securities->SelectedValue = $securities->item[0]->id;
                            $this->renderSecurityData($securities->item[0]->id);
                        }
                    } else {
                        if (isset($session['__currentsecurity__']) && isset($session['__currentsecurity__']->id)) {
                            $this->renderSecurityData($session['__currentsecurity__']->id);
                            $this->securities->SelectedValue = $session['__currentsecurity__']->id;
                        } else {
                            $this->securities->SelectedValue = $securities->item[0]->id;
                            $this->renderSecurityData($securities->item[0]->id);
                        }
                    }
                }


                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }
        }
    }

    private function renderSecurityData($secId)
    {
        $session = Prado::getApplication()->getSession();
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);


        //Get the details
        if ($secId != null) {
            $sd = $webservice->getWebService()->findSecurityOverviewById($secId);
            $session['__currentsecurity__'] = $sd;
        }

    }


    public function updateSecurityOverview($sender, $param)
    {
        $this->renderSecurityData($sender->SelectedValue);
    }


    public function showCArea($sender, $param)
    {
        $this->CArea->Visible = true;
        $this->CSpline->Visible = false;
        $this->CArea->render($param->getNewWriter());
    }

    public function showCSpline($sender, $param)
    {
        $this->CArea->Visible = false;
        $this->CSpline->Visible = true;
        $this->CSpline->render($param->getNewWriter());
    }

    public function updateChart($sender, $param)
    {
        $sid = $sender->ID;
        if ($sid == "CAreaButton") {
            $this->CArea->Visible = true;
            $this->CArea->render($param->getNewWriter());
            //Hide others
            $this->CSpline->Visible = false;
            $this->VArea->Visible = false;
            $this->VBar->Visible = false;
            $this->OHLC->Visible = false;
            $this->OHLCV->Visible = false;
        } else if ($sid == "CSplineButton") {
            $this->CSpline->Visible = true;
            $this->CSpline->render($param->getNewWriter());
            //Hide others
            $this->CArea->Visible = false;
            $this->VArea->Visible = false;
            $this->VBar->Visible = false;
            $this->OHLC->Visible = false;
            $this->OHLCV->Visible = false;
        } else if ($sid == "VAreaButton") {
            $this->VArea->Visible = true;
            $this->VArea->render($param->getNewWriter());
            //Hide others
            $this->CArea->Visible = false;
            $this->CSpline->Visible = false;
            $this->VBar->Visible = false;
            $this->OHLC->Visible = false;
            $this->OHLCV->Visible = false;
        } else if ($sid == "VBarButton") {
            $this->VBar->Visible = true;
            $this->VBar->render($param->getNewWriter());
            //Hide others
            $this->CArea->Visible = false;
            $this->VArea->Visible = false;
            $this->CSpline->Visible = false;
            $this->OHLC->Visible = false;
            $this->OHLCV->Visible = false;
        } else if ($sid == "OHLCButton") {
            $this->OHLC->Visible = true;
            $this->OHLC->render($param->getNewWriter());
            //Hide others
            $this->CArea->Visible = false;
            $this->VArea->Visible = false;
            $this->CSpline->Visible = false;
            $this->VBar->Visible = false;
            $this->OHLCV->Visible = false;
        } else if ($sid == "OHLCVButton") {
            $this->OHLCV->Visible = true;
            $this->OHLCV->render($param->getNewWriter());
            //Hide others
            $this->CArea->Visible = false;
            $this->VArea->Visible = false;
            $this->CSpline->Visible = false;
            $this->VBar->Visible = false;
            $this->OHLC->Visible = false;
        }


        //$this->renderSecurityData($sender->SelectedValue);
    }

}

?>