<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */
Prado::using('System.Web.UI.ActiveControls.*');

class ForgotPassword extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        $session = Prado::getApplication()->getSession();
        $session->open();
    }

    public function viewChanged($sender, $param)
    {

        $session = Prado::getApplication()->getSession();
        $session->open();

        if ($this->PasswordChange->ActiveViewIndex == 0) {
            //do nothing
        } else  if ($this->PasswordChange->ActiveViewIndex === 1 && $this->IsPostBack) {
            $pn = $this->portalUserName->Text;
            $webservice = new WebServiceClient(
                Prado::getApplication()->Parameters['mcs-wsdl'],
                Prado::getApplication()->Parameters['ws-username'],
                Prado::getApplication()->Parameters['ws-password']);

            $customer = $webservice->getWebService()->findCustomerByPortalUserName(strtolower($pn));
            $this->secretQuestion->Text = !isset($customer->secretQuestion) ? "Not Available" : $customer->secretQuestion;
            $session['__customer__'] = $customer;
        } else  if ($this->PasswordChange->ActiveViewIndex === 2 && $this->IsPostBack) {
            //verify the secret answer
        } else  if ($this->PasswordChange->ActiveViewIndex === 3 && $this->IsPostBack) {
            //update the password
            $customer = $session['__customer__'];
            $webservice = new WebServiceClient(
                Prado::getApplication()->Parameters['mcs-wsdl'],
                Prado::getApplication()->Parameters['ws-username'],
                Prado::getApplication()->Parameters['ws-password']);

            try {
                $webservice->getWebService()->updateCustomerPassword($customer->id, null, $this->password->Text);
            } catch (SoapFault $e) {
                //todo redirect
            }
        }
    }


    public function validateSecretAnswer($sender, $param)
    {
        $session = Prado::getApplication()->getSession();
        $session->open();
        $ob = $session['__customer__'];

        if (isset($ob->secretAnswer) && strtolower($ob->secretAnswer) === strtolower($param->Value)) {
            $param->IsValid = true;
        } else {
            $param->IsValid = false;
        }
    }

    public function validateUsername($sender, $param) {
        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);

        $param->IsValid = false; //Default to false and then switch to true if we get a SOAP Fault
        try {
            $webservice->getWebService()->findCustomerByPortalUserName(strtolower($param->Value));
            $param->IsValid = true;
        } catch (SoapFault $e) {
            $param->IsValid = false;
        }
    }

}

?>