<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

class CashStatement extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Account Summary - " . $this->Application->Session['__customer__']->label;

		
	
   
    }
	
	
	public function PrintCashStatement($sender,$param)
	{
		$session = Prado::getApplication()->getSession();
		
//		$url = "https://apps-demo.zanibal.com/ebclient/rest/api/v1/finance/filedger/list/account/id?a=29&b=0&c=1000&sd=".$startDate."&ed=".$endDate;
//		$cashStatement = Util::getJSONfromURL($url);
//		$portfolioId = $this->jsonPortfolios->SelectedValue;
		
		
		$startDate = $this->StartDate->Text;
		$endDate = $this->EndDate->Text;
		
		
		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);
		
		$cashStatement = $webservice->getWebService()->findCustomerLedgerEntries($session['__customer__']->id, 0, 10000,  $startDate ,  $endDate);
		
		$session['__cashStatementPdf__'] = $cashStatement->item;
		$session['__startDate__'] = $startDate;
		$session['__endDate__'] = $endDate;
		
		//die(Util::convertToJavaDate($startDate));
		//die(print_r( $cashStatement));
		$url = "pdf/CashStatementOutput.php";
		$this->Response->redirect($url);
		
		///die(print_r($data));
		
		
		//die($portfolioId.'/'.$startDate.'/'.$endDate);
	}
	
	public function bindFormValues($ob)
	{
		
	}

}

?>