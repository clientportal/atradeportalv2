
<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

class DepositFunds extends TPage
{


	public function onInit($param)
	{
		parent::onInit($param);

		//Set the page title
		$this->Page->Title = "ClientPortal - Deposit Funds - " . $this->Application->Session['__customer__']->label;


		if (!$this->IsPostBack) // if the page is requested the first time
		{
			$session = Prado::getApplication()->getSession();
			try {
				$webservice = new WebServiceClient(
					Prado::getApplication()->Parameters['mcs-wsdl'],
					Prado::getApplication()->Parameters['ws-username'],
					Prado::getApplication()->Parameters['ws-password']);

				//Get the accounts
				$accounts = $webservice->getWebService()->findCustomerFiAccts($session['__customer__']->id);
				//die(print_r($accounts));
				$session['__accounts__'] = $accounts;
				$this->AccountRepeater->DataSource = $accounts->item;
				$this->AccountRepeater->dataBind();

				$this->account->DataSource = $accounts->item;
				$this->account->dataBind();

				//Reset messages
				$session['__deposit_success__'] = false;
				$session['__deposit_message__'] = "";
			} catch (SoapFault $e) {
				throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
			}

		}


	}


	public function viewChanged($sender, $param)
	{

		$session = Prado::getApplication()->getSession();
		$session->open();
		$this->AccountRepeater->DataSource = $session['__accounts__']->item;
		$this->AccountRepeater->dataBind();
		//die($this->DepositFunds->ActiveViewIndex);
		$ob = $session['__deposit_doc__'];
		if ($ob == null) {
			$ob = new PartnerCashTransactionBean();
			$session['__deposit_doc__'] = $ob;
		}

		if ($this->DepositFunds->ActiveViewIndex == 0) {
			//do nothing
		} else  if ($this->DepositFunds->ActiveViewIndex === 1 && $this->IsPostBack) {
			$this->bindFormValues($ob);
			$this->createPartnerCashTransaction($ob);

			//convenience Fee Calculation
			$session['__Deposite_convenienceFee__'] =  $this->convenienceFee($ob->amount);
			$session['__deposite_totalFee__'] =  $ob->amount + $this->convenienceFee($ob->amount);
			//Generate the hash
			$ghamount = $session['__deposite_totalFee__'] * 100;

			$hash_key = $ob->name . Prado::getApplication()->Parameters['payment-gateway-product-id'] . Prado::getApplication()->Parameters['payment-gateway-item-id'] . $ghamount . Prado::getApplication()->Parameters['payment-gateway-redirect-url'] . Prado::getApplication()->Parameters['payment-gateway-key'];
			$ob->hash = hash("sha512", $hash_key);

		} else  if ($this->DepositFunds->ActiveViewIndex === 2 && $this->IsPostBack) {
			//create the PCN and send to interswitch
			//$this->createPartnerCashTransaction($ob);
			//Generate the hash
			$ghamount = $session['__deposite_totalFee__']  * 100;
			$hash_key = $ob->name . Prado::getApplication()->Parameters['payment-gateway-product-id'] . Prado::getApplication()->Parameters['payment-gateway-item-id'] . $ghamount . Prado::getApplication()->Parameters['payment-gateway-redirect-url'] . Prado::getApplication()->Parameters['payment-gateway-key'];
			$ob->hash = hash("sha512", $hash_key);
		}
	}


	public function bindFormValues($ob)
	{
		$session = Prado::getApplication()->getSession();

		//$ob->amount = Util::formatNumber($this->amount->Text, 2);
		$ob->amount = $this->amount->Text;
		$ob->accountId = $this->account->SelectedValue;
		//Get the account label
		$accounts = $session['__accounts__'];
		foreach ($accounts->item as $i) {
			if ($ob->accountId == $i->id) {
				$ob->accountLabel = $i->label;
				break;
			}
		}

		//Bind values from the customer object and other static values
		$ob->partnerId = $session['__customer__']->id;
		$ob->partnerName = $session['__customer__']->name;
		$ob->partnerLabel = $session['__customer__']->label;
		$ob->currency = "NGN";
		$ob->transactionDate = Util::getTodaysDate(null);
		$ob->valueDate = Util::getTodaysDate(null);
		$ob->transType = "RECEIPT";
		$ob->transMethod = "ECHANNEL";
		$ob->transState = "PENDING";
	}

	private function createPartnerCashTransaction($ob)
	{
		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		if ($ob->id == null) {
			//we will now create the transaction
			$doc = $this->buildSOAPDocument($ob);
			$id = $webservice->getWebService()->createPartnerCashTransaction($doc);
			$ndoc = $webservice->getWebService()->findPartnerCashTransactionById($id);
			$ob->id = $ndoc->id;
			$ob->name = $ndoc->name;
		} else {
			$webservice->getWebService()->updatePartnerCashTransactionAmountAndAccount($ob->id, $ob->amount, $ob->accountId);
		}
	}


	private function buildSOAPDocument($doc)
	{
		$doc = array(
			"transactionDate" => $doc->transactionDate,
			"valueDate" => $doc->valueDate,
			"transState" => $doc->transState,
			"businessOfficeName" => $doc->businessOfficeName,
			"transType" => $doc->transType,
			"transMethod" => $doc->transMethod,
			"amount" => $doc->amount,
			"currency" => $doc->currency,
			"partnerId" => $doc->partnerId,
			"partnerName" => $doc->partnerName,
			"cashAccountId" => $doc->accountId,
			"paymentGatewayId" => Prado::getApplication()->Parameters['payment-gateway-id'],
			"reference" => null,
			"contraAcctId" => null
		);
		return $doc;
	}

	public function convenienceFee($amount)
	{
		$value = $amount * 0.015;
		if($value < 1500)
			return $value;
		else
			return 1500;
	}
}

?>