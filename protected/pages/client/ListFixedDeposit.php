<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.

 */
//ini_set('error_reporting', E_ALL);

Prado::using('System.Web.UI.ActiveControls.*');
class  ListFixedDeposit extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Equity Order List - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {

                $td = Util::getTodaysDate(null);
                $sd = Util::addDaysToDate($td, -30);
                $ed = Util::addDaysToDate($td, 1);

                $this->StartDate->Text = $sd;
                $this->EndDate->Text = $ed;

                $this->OrderStatus->SelectedValue = "";
                $this->renderOrderList(null, null, $sd, $ed);
			$webservice = new WebServiceClient(
					Prado::getApplication()->Parameters['mcs-wsdl'],
					Prado::getApplication()->Parameters['ws-username'],
					Prado::getApplication()->Parameters['ws-password']);
                 if($session['__termInstrument__'] ==  null){
					$termInstrument = $webservice->getWebService()->findActiveTermInstrumentTypes();
//die(print_r( $termInstrument.'----a' ));
					foreach($termInstrument->item as $item) {
						if($item->fundsOwner = 'PARTNER'){
							$items[] = $item;
						}
					}
					$session['__termInstrument__'] =  $items;
				}
				//Get the Term Instrument Products
				$this->TermInstrumentType->DataSource = array('Instrument'=>'') + $session['__termInstrument__'];
				$this->TermInstrumentType->dataBind();
               
		//Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }
		
		
		
		


    }

    private function renderOrderList($type, $status, $startDate, $endDate)
    {
        $session = Prado::getApplication()->getSession();

        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);


        //Get the order list
        if($status == "ALL") $status = null;
		if(!isset($type)) $type = null;
		$orderList = $webservice->getWebService()->findCustomerTermInstruments($session['__customer__']->id,
                                                                                    0, 1000000, $type, $status, Util::convertToJavaDate($startDate),
                                                                                    Util::convertToJavaDate($endDate));
	//	die(print_r($orderList));

        $ol = isset($orderList->item) ? $orderList->item : array();
       $this->OrderListRepeater->DataSource = $ol;
        $this->OrderListRepeater->dataBind();


    }

    public function updateTradeOrderListPage($sender, $param)
    {
        $sd = $this->StartDate->Text;
        $ed = $this->EndDate->Text;
        $status = $this->OrderStatus->SelectedValue;
       // $type = $this->TermInstrumentType->SelectedValue;
$type = null;
//die($type.$status);		
        $this->renderOrderList($type, $status, $sd, $ed);

    }
}

?>
