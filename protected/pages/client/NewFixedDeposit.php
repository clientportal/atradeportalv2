<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */

Prado::using('System.Web.UI.ActiveControls.*');

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

class NewFixedDeposit extends TPage
{
	public function onInit($param)
	{
		parent::onInit($param);

		$session = Prado::getApplication()->getSession();
		//Set the page title
		$this->Page->Title = "ClientPortalMTrader - Fixed Deposit - " . $this->Application->Session['__customer__']->label;



		if (!$this->IsPostBack) // if the page is requested the first time
		{
			//$session = Prado::getApplication()->getSession();
		}


	}

	public function viewChanged($sender, $param)
	{
		$session = Prado::getApplication()->getSession();

		//Create and save in session if this is the first time.
		$ob = $session['__fixedDeposit__'];
		if ($ob == null) {
			$ob = new FixedDeposit();
			$session['__fixedDeposit__'] = $ob;
		}

		//die(print_r($session['__fixedDeposit__']));

		try {

			$webservice = new WebServiceClient(
				Prado::getApplication()->Parameters['mcs-wsdl'],
				Prado::getApplication()->Parameters['ws-username'],
				Prado::getApplication()->Parameters['ws-password']);

			if ($this->FixedDeposit->ActiveViewIndex == 0){

				if($session['__termInstrument__'] ==  null){
					$termInstrument = $webservice->getWebService()->findActiveTermInstrumentTypes();

					foreach($termInstrument->item as $item) {
						if($item->fundsOwner = 'PARTNER'){
							$items[] = $item;
						}
					}
					$session['__termInstrument__'] =  $items;
				}
				//Get the Term Instrument Products
				$this->TermInstrumentType->DataSource = array(''=>'') + $session['__termInstrument__'];
				$this->TermInstrumentType->dataBind();

				//Get the portfolio list
				$portfolios = $session['__portfolios__'];
				if ($portfolios == null) {
					$portfolios = $webservice->getWebService()->findCustomerPortfolios($session['__customer__']->id);
					$session['__portfolios__'] = $portfolios;
				}
				$this->Portfolio->DataSource = $session['__portfolios__']->item;
				$this->Portfolio->dataBind();

			}
			elseif ($this->FixedDeposit->ActiveViewIndex === 1 && $this->IsPostBack) {

				$this->bindFormValues($ob);

			}else{
			
				$id = $this->createFixedDeposit($ob);
				//die(print_r($id));

				$response = $webservice->getWebService()->findTermInstrumentById($id);
				//die(print_r($response));
				$session['__fixedDeposit__currency'] = $response->currency ;
				$session['__fixedDeposit__defaultRate']  = $response->currentRate;
				$session['__fixedDeposit__label'] = $response->customerLabel;
				$session['__fixedDeposit__expectedInterest'] = $response->expectedInterest;
				$session['__fixedDeposit__expectedMaturity'] = $response->expectedMaturity;
				$session['__fixedDeposit__expectedNetInterest'] = $response->expectedNetInterest;
				$session['__fixedDeposit__faceValue'] = $response->faceValue;
				$session['__fixedDeposit__instrumentTypeLabel'] = $response->instrumentTypeLabel;
				$session['__fixedDeposit__name'] = $response->name;
				$session['__fixedDeposit__portfolio'] = $response->portfolioLabel ;
				$session['__fixedDeposit__rolloverRule'] = $response->rolloverRule ;
				$session['__fixedDeposit__startDate'] = $response->startDate;
				$session['__fixedDeposit__status']= $response->status;
				$session['__fixedDeposit__tenure'] = $response->tenure;
				$session['__fixedDeposit__id'] = $response->id;
				//die(print_r($ob));

				//}
			}

		} catch (SoapFault $e) {
			throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
		}

	}

	public function createFixedDeposit($ob){

		$session = Prado::getApplication()->getSession();
		try {

			$webservice = new WebServiceClient(
				Prado::getApplication()->Parameters['mcs-wsdl'],
				Prado::getApplication()->Parameters['ws-username'],
				Prado::getApplication()->Parameters['ws-password']);
			$data = array(
				'currency' => $ob->currency ,
				'autoRollover' => 1, //true
				'currentRate' => $ob->defaultRate,
				'customerId' => $session['__customer__']->id,
				'faceValue' => $ob->faceValue,
				'instrumentTypeName' => $ob->name,
				'portfolioName' => $ob->portfolio,
				'rolloverRule' => $ob->rolloverRule,
				'startDate' => Util::convertToJavaDate($ob->startDate),
				'tenure' => $ob->tenure
			);

			//die(print_r($data));
			$id = $webservice->getWebService()->createTermInstrument($data);
			return $id;

		} catch (SoapFault $e) {
			throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
		}
	}

	public function findTermInstrumentById($id, $ob){

		$session = Prado::getApplication()->getSession();
		try {

			$webservice = new WebServiceClient(
				Prado::getApplication()->Parameters['mcs-wsdl'],
				Prado::getApplication()->Parameters['ws-username'],
				Prado::getApplication()->Parameters['ws-password']);



			return $ob;

		} catch (SoapFault $e) {
			throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
		}
	}

	public function bindFormValues($ob)
	{
		$ob->rolloverRule = $this->RolloverRule->Text;
		$ob->faceValue = $this->AmountInvested->Text;
		$ob->portfolio = $this->Portfolio->SelectedValue;
		$ob->startDate = $this->StartDate->Text;
		//die(print_r($ob));
		$ob->tenure = $this->TenureValue->Text != $ob->tenure ? $this->TenureValue->Text : $ob->tenure;
		
		return $ob;
	}


	public function DBDropDownList3Changed($sender,$param)
	{
		$session = Prado::getApplication()->getSession();
		$ob = $session['__fixedDeposit__'];	
		foreach($session['__termInstrument__'] as $result){
			if($result->id == $this->TermInstrumentType->SelectedValue ){

				$ob->currency = $result->currency;
				$ob->defaultRate = $result->defaultRate;
				$ob->name  = $result->name;
				$ob->label = $result->label;
				$ob->tenure  =  $this->TenureValue->Text = $result->defaultTenure;
				$ob->canChangeTenure = $result->canChangeTenure;
				break;
			}
		}
		return $ob;
	}

	public function findTermInstrumentTypeById($termId){
		$session = Prado::getApplication()->getSession();
		
		
	}



}

?>