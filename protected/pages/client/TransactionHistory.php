<?php
/**
 * Created by IntelliJ IDEA.
 * User: uigwebuike
 * Date: 6/7/12
 * Time: 7:30 AM
 * To change this template use File | Settings | File Templates.
 */
Prado::using('System.Web.UI.ActiveControls.*');

class TransactionHistory extends TPage
{


    public function onInit($param)
    {
        parent::onInit($param);

        //Set the page title
        $this->Page->Title = "ClientPortal - Transaction List - " . $this->Application->Session['__customer__']->label;


        if (!$this->IsPostBack) // if the page is requested the first time
        {
            $session = Prado::getApplication()->getSession();
            try {

                $accounts = $session['__accounts__'];
                if ($accounts == null) {
                    $webservice = new WebServiceClient(
                        Prado::getApplication()->Parameters['mcs-wsdl'],
                        Prado::getApplication()->Parameters['ws-username'],
                        Prado::getApplication()->Parameters['ws-password']);

                    $accounts = $webservice->getWebService()->findCustomerFiAccts($session['__customer__']->id);
                    $session['__accounts__'] = $accounts;
                }

                $td = Util::getTodaysDate(null);
                $sd = Util::addDaysToDate($td, -30);
                $ed = Util::addDaysToDate($td, 1);

                $this->StartDate->Text = $sd;
                $this->EndDate->Text = $ed;

                if (count($accounts->item) > 0) {
                    $this->accounts->DataSource = $accounts->item;
                    $this->accounts->dataBind();

                    $this->accounts->SelectedValue = $accounts->item[0]->id;
                    $this->renderTransactionsList($accounts->item[0]->id, $sd, $ed);
                }

                //Prado::log(print_r($cust), TLogger::ERROR, 'AppException');
            } catch (SoapFault $e) {
                throw new AppException(500, 'Unable to process request - ' . $e->faultstring);
            }

        }


    }

    private function renderTransactionsList($accountId, $startDate, $endDate)
    {
        $session = Prado::getApplication()->getSession();

        $webservice = new WebServiceClient(
            Prado::getApplication()->Parameters['mcs-wsdl'],
            Prado::getApplication()->Parameters['ws-username'],
            Prado::getApplication()->Parameters['ws-password']);


        //Get the GL entries -- todo add th date filters
        $glList = $webservice->getWebService()->findFiAcctLedgerEntries($accountId, 0, 1000, Util::convertToJavaDate($startDate), Util::convertToJavaDate($endDate));

        $ol = isset($glList->item) ? $glList->item : array();
        $this->TransactionHistoryListRepeater->DataSource = $ol;
        $this->TransactionHistoryListRepeater->dataBind();

    }

	
	
	public function PrintCashStatement($sender,$param)
	{
		$session = Prado::getApplication()->getSession();

	
		$startDate = $this->StartDate->Text;
		$endDate = $this->EndDate->Text;


		$webservice = new WebServiceClient(
			Prado::getApplication()->Parameters['mcs-wsdl'],
			Prado::getApplication()->Parameters['ws-username'],
			Prado::getApplication()->Parameters['ws-password']);

		$cashStatement = $webservice->getWebService()->findCustomerLedgerEntries($session['__customer__']->id, 0, 10000,  $startDate ,  $endDate);
		if($cashStatement->item){
		$session['__cashStatementPdf__'] = $cashStatement->item;
		$session['__startDate__'] = $startDate;
		$session['__endDate__'] = $endDate;
		$session['__username__'] = $session['__customer__']->name;
		}else{
			$session['__message__'] = 'Your Cash Statement is Empty';
		}
	}

    public function updateTransactionHistoryPage($sender, $param)
    {
        $sd = $this->StartDate->Text;
        $ed = $this->EndDate->Text;
        $accountId = $this->accounts->SelectedValue;
        $this->renderTransactionsList($accountId, $sd, $ed);
    }
}

?>