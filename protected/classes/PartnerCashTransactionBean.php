<?php

/**
 * @throws AppException
 * This class deals with capturing data and managing the lead to quote process
 */
class PartnerCashTransactionBean
{
    public $id;
    public $name;
    public $amount;
    public $currency;
    public $accountId;
    public $accountLabel;
    public $partnerId;
    public $partnerName;
    public $hash;
    public $transactionDate;
    public $valueDate;
    public $businessOfficeName;
    public $transType;
    public $transMethod;
    public $transState;






    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    public function getAccountId()
    {
        return $this->accountId;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setBusinessOfficeName($businessOfficeName)
    {
        $this->businessOfficeName = $businessOfficeName;
    }

    public function getBusinessOfficeName()
    {
        return $this->businessOfficeName;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
    }

    public function getPartnerId()
    {
        return $this->partnerId;
    }

    public function setTransMethod($transMethod)
    {
        $this->transMethod = $transMethod;
    }

    public function getTransMethod()
    {
        return $this->transMethod;
    }

    public function setTransState($transState)
    {
        $this->transState = $transState;
    }

    public function getTransState()
    {
        return $this->transState;
    }

    public function setTransType($transType)
    {
        $this->transType = $transType;
    }

    public function getTransType()
    {
        return $this->transType;
    }

    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;
    }

    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    public function setValueDate($valueDate)
    {
        $this->valueDate = $valueDate;
    }

    public function getValueDate()
    {
        return $this->valueDate;
    }

    public function setAccountLabel($accountLabel)
    {
        $this->accountLabel = $accountLabel;
    }

    public function getAccountLabel()
    {
        return $this->accountLabel;
    }

    public function setPartnerName($partnerName)
    {
        $this->partnerName = $partnerName;
    }

    public function getPartnerName()
    {
        return $this->partnerName;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    public function getHash()
    {
        return $this->hash;
    }


}

?>