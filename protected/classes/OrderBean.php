<?php

/**
 * @throws AppException
 * This class deals with capturing data and managing the lead to quote process
 */
class OrderBean
{
    public $id;
    public $name;
    public $portfolioName;
    public $portfolioLabel;
    public $securityName;
    public $limitPrice;
    public $stopPrice;
    public $priceType;
    public $orderOrigin = "WEB";
    public $orderType;
    public $quantityRequested;
    public $orderCurrency;
    public $orderTermName;
    public $orderTermLabel;
    public $orderCost;
    public $orderDate;



    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }




    public function setLimitPrice($limitPrice)
    {
        $this->limitPrice = $limitPrice;
    }

    public function getLimitPrice()
    {
        return $this->limitPrice;
    }

    public function setOrderCurrency($orderCurrency)
    {
        $this->orderCurrency = $orderCurrency;
    }

    public function getOrderCurrency()
    {
        return $this->orderCurrency;
    }

    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;
    }

    public function getOrderDate()
    {
        return $this->orderDate;
    }

    public function setOrderOrigin($orderOrigin)
    {
        $this->orderOrigin = $orderOrigin;
    }

    public function getOrderOrigin()
    {
        return $this->orderOrigin;
    }

    public function setOrderTermName($orderTermName)
    {
        $this->orderTermName = $orderTermName;
    }

    public function getOrderTermName()
    {
        return $this->orderTermName;
    }

    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }

    public function getOrderType()
    {
        return $this->orderType;
    }

    public function setPortfolioName($portfolioName)
    {
        $this->portfolioName = $portfolioName;
    }

    public function getPortfolioName()
    {
        return $this->portfolioName;
    }

    public function setPriceType($priceType)
    {
        $this->priceType = $priceType;
    }

    public function getPriceType()
    {
        return $this->priceType;
    }

    public function setQuantityRequested($quantityRequested)
    {
        $this->quantityRequested = $quantityRequested;
    }

    public function getQuantityRequested()
    {
        return $this->quantityRequested;
    }

    public function setSecurityName($securityName)
    {
        $this->securityName = $securityName;
    }

    public function getSecurityName()
    {
        return $this->securityName;
    }

    public function setStopPrice($stopPrice)
    {
        $this->stopPrice = $stopPrice;
    }

    public function getStopPrice()
    {
        return $this->stopPrice;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setOrderTermLabel($orderTermLabel)
    {
        $this->orderTermLabel = $orderTermLabel;
    }

    public function getOrderTermLabel()
    {
        return $this->orderTermLabel;
    }

    public function setPortfolioLabel($portfolioLabel)
    {
        $this->portfolioLabel = $portfolioLabel;
    }

    public function getPortfolioLabel()
    {
        return $this->portfolioLabel;
    }

    public function setOrderCost($orderCost)
    {
        $this->orderCost = $orderCost;
    }

    public function getOrderCost()
    {
        return $this->orderCost;
    }
}

?>