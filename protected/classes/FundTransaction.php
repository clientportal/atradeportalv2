<?php

/**
 * @throws AppException
 * This class deals with capturing data and managing the lead to quote process
 */
class FundTransaction
{
    public $id;
	
    public $name;
    public $label;
    public $transType;
    public $orderBase;

    public $fundId;
    public $fundName;
    public $fundLabel;

    public $customerId;

    public  $orderDate;
	
    public $transUnits;
    public $transAmount;
	public $currency;
	public $description;

	public $portfolioId;
	public $portfolioName;
	public $portfolioLabel;


	public function setPortfolioLabel($portfolioLabel)
    {
		$this->portfolioLabel = $portfolioLabel;
    }
	

	public function getPortfolioLabel()
	{
		return $this->portfolioLabel;
	}
	
	
	public function setPortfolioName($portfolioName)
    {
		$this->portfolioName = $portfolioName;
    }

	public function getPortfolioName()
	{
		return $this->portfolioName;
	}
	
	public function setPortfolioId($portfolioId)
    {
		$this->portfolioId = $portfolioId;
    }
	

	public function getPortfolioId()
	{
		return $this->portfolioId;
	}
	
	
	
	public function setCurrency($currency)
    {
		$this->currency = $currency;
    }
	

	public function getCurrency()
	{
		return $this->currency;
	}


	public function setTransAmount($transAmount)
    {
		$this->transAmount = $transAmount;
    }
	

	public function getTransAmount()
	{
		return $this->transAmount;
	}




	public function setTransUnits($transUnits)
    {
		$this->transUnits = $transUnits;
    }
	public function getTransUnits()
	{
		return $this->transUnits;
	}

	
	public function setOrderDate($orderDate)
    {
		$this->orderDate = $orderDate;
    }
	public function getOrderDate()
	{
		return $this->orderDate;
	}

	public function setDescription($description)
    {
		$this->description = $description;
    }
	public function getDescription()
	{
		return $this->description;
	}

	


	public function setCustomerId($customerId)
    {
		$this->customerId = $customerId;
    }
	

	public function getCustomerId()
	{
		return $this->customerId;
	}
	
	public function setFundName($fundName)
    {
		$this->fundName = $fundName;
    }
	
	public function getFundName()
	{
		return $this->fundName;
	}
	
	
	

	
   



   
}

?>